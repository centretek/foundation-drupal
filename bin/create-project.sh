#!/usr/bin/env bash
set -eux
shopt -s dotglob

function initLando() {
  mkdir docroot
  ln -s docroot web
  lando init \
    --source cwd \
    --recipe drupal9 \
    --webroot docroot

  cat <<-EOF >> .lando.yml
env_file:
  - .env
services:
  appserver:
    run:
# Lando leaves all user identity files in /user/.ssh, but ssh config files present in that directory are not used
# because the home directory Lando uses is (usually) in /var/www. Since some folks may have specified certain identity
# files per host, we copy the ssh config to the home directory in the Lando container. If ssh config has references to
# identity files like ~/.ssh/id_rsa_custom, Lando's container will interpret that as ~ referring to /var/www, but the
# identity files are in /user, so we need to copy them to /var/www.
      - rsync -ravz --exclude 'known_hosts' /user/.ssh/ /var/www/.ssh
EOF

  lando start
}

function installDrupal() {
  lando composer create-project centretek/foundation-drupal \
    --repository="{\"type\":\"vcs\",\"url\":\"https://bitbucket.org/centretek/foundation-drupal.git\"}" \
    --stability=dev \
    --remove-vcs
  mv foundation-drupal/* .
  rm -rf foundation-drupal
  rm -rf bin
  cp assets/services.yml docroot/sites/default/services.yml
  lando drush site:install \
    standard \
    --yes \
    --db-url=mysql://drupal9:drupal9@database:3306/drupal9
}

function postProcessSettings() {
  # Drush site-install puts config stuff in settings.php. Local config should instead be in settings.local.php.
  chmod u+w docroot/sites/default
  chmod u+w docroot/sites/default/settings.php
  rm docroot/sites/default/settings.php
  cp docroot/sites/default/default.settings.php docroot/sites/default/settings.php
  cat <<-EOF >> docroot/sites/default/settings.local.php
<?php
// Only needed locally, as Pantheon and Acquia supply their own.
\$settings['hash_salt'] = '$(lando drush eval "echo Drupal\Component\Utility\Crypt::randomBytesBase64(55);")';
// Make your changes below:
EOF
  chmod u-w docroot/sites/default
  chmod u-w docroot/sites/default/settings.php
}

initLando
installDrupal
postProcessSettings
