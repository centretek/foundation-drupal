<?php

use Centretek\Settings\SettingsFactory;

SettingsFactory::create(
  $app_root,
  $site_path,
  $settings,
  $databases,
  $config
)
  ->withDefaults()
;
